# Привет! :wave:

Меня зовут Артём Соколовский, full-stack web-разработчик, индивидуальный предприниматель с 4 октября 2018, 25 лет из
Саратова. О моих услугах можете узнать на моём сайте [sokolovsky.dev](https://sokolovsky.dev/), там же почитать мои
статьи.

## Опыт
Работаю с 2013 года web-разработчиком, начинал обычным фрилансером, со временем собрал собственную команду
профессионалов своего дела, от дизайнера до верстальщиков и тестировщиков. Организовал собственную web-студию и работал
до мая 2021 года. В данный момент занимаюсь долгосрочными проектами, автоматизирую бизнес процессы, помогаю
предпринимателям строить свой бизнес в интернете без штрафов и проблем. Веду проекты от разработки до дальнейшей
поддержки, реализовываю от интернет-магазинов, до персональных CRM систем и REST API. В свободное время пишу пакеты для 
Yii2 [c7v/yii-netangels](https://github.com/c7v/yii-netangels) и [c7v/yii_yandex_turbo_pages_api](https://github.com/c7v/yii_yandex_turbo_pages_api).


Зарегистрировал собственное СМИ, свидетельство о регистрации ЭЛ № ФС 77 - 81443 от 15.07.2021 года. Вы можете почитать
мои интересные статьи и статьи моих журналистов :heart: на [temj.ru](https://temj.ru/). В данный момент занимаюсь SEO 
оптимизацией Temj и полной разработкой проекта.

## Работа
Работаю на [framework Yii2](https://www.yiiframework.com/) :heart: basic и advanced, использую этот framework 95%,
[framework Laravel](https://laravel.ru/) начинаю использовать в своей работе. Всегда использую GIT, для контроля версий
проектов. Самостоятельно администрирую linux-сервера.


<u>Избегаю работы на различных CMS, такие проекты не интересуют.</u>

## Контакты
:mailbox: E-mail: artem@sokolovsky.dev, есть PGP ключ [737BFC41EC0CD9C1](https://github.com/c7v/c7v/blob/master/pgp/737BFC41EC0CD9C1.asc).

:telephone: Телефон: +7 995 499 5990

:speech_balloon: Телеграмм: [@devSokolovsky](https://t.me/devSokolovsky)
